```bash
install dependencies:
  $ cd nullcorpus-api && npm install

run the app:
  $ DEBUG=nullcorpus-api:* npm start
```
