var configDB = {
  'url' : 'mongodb://127.0.0.1:3001/meteor'
};

var MongoClient = require('mongodb').MongoClient;
var db;

module.exports = function(callback) {
  if (db) {
    callback(null, db);
  }
  else {
    MongoClient.connect(configDB.url, function(error, database) {
      db = database;
      callback(error, db);
    });
  }
}
